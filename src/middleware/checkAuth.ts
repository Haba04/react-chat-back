import verifyJWTToken from '../utils/verifyJWTToken';

export default (req: any, res: any, next: any) => {
    if (req.path === '/users/login' || req.path === '/users/registration') {
        return next();
    } else {
        const token = req.headers.token;
        verifyJWTToken(token).then((user: any) => {
            req.user = user.data._doc;
            next();
        }).catch(() => {
            res.status(403).json({message: "Токен неверный"})
        });
    }
}