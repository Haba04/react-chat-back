import {Request, Response, NextFunction} from 'express';
import UserModel from '../models/User';

export default (req: Request, res: Response, next: NextFunction) => {
    UserModel.updateOne(
        {_id: '5ecc220491ef1ebb1c3e362f'},
        {$set: {last_seen: new Date()}},
        () => {
        }
    );
    next();
}