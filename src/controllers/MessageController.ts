import express from 'express';
import MessageModel from '../models/Message'

/**
 * @class
 * MessageController содержит CRUD операции по модели User
 */
class MessageController {

    /**
     * Метод запрашивает диалоги с БД по id, если пользователь не найден, выдает статус 404 и сообщение
     * @param req - запрос
     * @param res - ответ
     * @return {json} - возвращает объект, содержащий {message: '...'}
     */
    index(req: express.Request, res: express.Response) {
        const dialogId = Object(req.params._id);
        MessageModel.find({dialog: dialogId})
            .populate(["dialog", 'author'])
            .exec((err, messages) => {
                if (err) {
                    return res.status(404).json({
                        message: 'Сообщения не найдены'
                    });
                }
                return res.json(messages);
            })
    }

    /**
     * Метод создает сообщение. В БД заносится только author, partner, остальные значение, пришедшие
     * с req.body игнорируются
     * @param {express.Request} req - запрос
     * @param {express.Response} res - ответ
     */
    create(req: express.Request, res: express.Response) {
        const userId = '5ecc220491ef1ebb1c3e362f';
        const postData = {
            text: req.body.text,
            dialog: req.body.dialog,
            author: userId
        };

        res.send(req.body);

        const message = new MessageModel(postData);
        message.save().then((object: any) => {
            res.json(object);
        }).catch((err: any) => {
            res.json(err);
        })
    }

    /**
     * Метод удаляет сообщение с БД. В случае успешного удаления получаем статус 200, неудачного - 404.
     * @param {express.Request} req - запрос
     * @param {express.Response} res - ответ
     */
    delete(req: express.Request, res: express.Response) {
        const _id: string = req.params._id;
        MessageModel.findOneAndRemove({_id})
            .then(message => {
                message && res.status(200).json({message: 'Сообщение успешно удалено'});
            }).catch(() => {
            res.status(404).json({message: 'Сообщение не найдено'})
        });
    }
}

export default MessageController;