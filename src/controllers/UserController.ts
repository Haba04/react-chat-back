import {Request, Response} from 'express';
import UserModel from '../models/User'
import createJWTToken from '../utils/createJWTToken'
import {validationResult} from "express-validator";
import {compareUserPassword} from "../utils/generateUserPassword";

/**
 * @class
 * DialogController содержит CRUD операции по модели User
 */
class UserController {

    /**
     * Метод запрашивает user-а с БД по id, если пользователь не найден, выдает статус 404 и сообщение
     * @param req - запрос
     * @param res - ответ
     * @return {json} - возвращает объект, содержащий {message: '...'}
     */
    show(req: Request, res: Response) {
        const id: string = req.params._id;
        UserModel.findById(id, (err, user) => {
            if (err) {
                return res.status(404).json({message: 'SHOW: Пользователь не найден'});
            }
            res.json(user);
        });
    }

    /**
     * Метод создает user-а. В БД заносится только email, fullname, password, остальные значение, пришедшие
     * с req.body игнорируются
     * @param {Request} req - запрос
     * @param {Response} res - ответ
     */
    create(req: Request, res: Response) {
        const postData = {
            email: req.body.email,
            fullname: req.body.fullname,
            password: req.body.password
        };

        res.send(req.body);

        const user = new UserModel(postData);
        user.save().then((object: any) => {
            res.json(object);
        }).catch((reason: any) => {
            res.json(reason);
        })
    }

    /**
     * Метод для авторизации. При правильном логине и пароле выдает токен.
     * @param {Request} req - запрос
     * @param {Response} res - ответ
     */
    login(req: Request, res: Response) {
        const postData = {
            email: req.body.email,
            password: req.body.password
        };

        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({errors: errors.array()});
        }

        UserModel.findOne({email: postData.email}, (err, user) => {
            if (err) {
                return res.status(404).json({
                    message: "LOGIN: Пользователь не найден"
                })
            }

            user && compareUserPassword(postData.password, user.password).then((result) => {
                if (result) {
                    const token = createJWTToken(user);
                    res.json({
                        status: 'success',
                        token
                    });
                } else {
                    res.json({
                        status: 'error',
                        message: "Неправильный логин или пароль"
                    })
                }
            }).catch((err) => {
                return res.status(404).json({
                    message: err
                })
            });
        });
    }

    /**
     * Метод удаляет user-а с БД. В случае успешного удаления получаем статус 200, неудачного - 404.
     * @param {Request} req - запрос
     * @param {Response} res - ответ
     */
    delete(req: Request, res: Response) {
        const _id: string = req.params._id;
        UserModel.findOneAndRemove({_id}, (err, user) => {
            if (err) {
                res.status(404).json({message: "Не получилось удалить пользователя"});
            }
            res.status(200).json({message: `User ${user && user.fullname} успешно удален`});
        })
    }

    /**
     * Метод возвращает информацию об авторизованном пользователе.
     * @param {Request} req - запрос
     * @param {Response} res - ответ
     */
    getMe(req: any, res: Response) {
        const id: string = req.user._id;
        UserModel.findById(id, (err, user) => {
            if (err) {
                console.log('err:', err);
                return res.status(404).json({
                    message: "GetMe: Пользователь не найден"
                })
            }
            res.json(user);
        })
    }
}

export default UserController;