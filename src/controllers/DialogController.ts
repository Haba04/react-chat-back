import express from 'express';
import DialogModel from '../models/Dialog'
import MessageModel from '../models/Message';

/**
 * @class
 * DialogController содержит CRUD операции по модели User
 */
class DialogController {

    /**
     * Метод запрашивает все диалоги с БД по id, если пользователь не найден, выдает статус 404 и сообщение
     * @param req - запрос
     * @param res - ответ
     * @return {json} - возвращает объект, содержащий {message: '...'}
     */
    index(req: any, res: express.Response) {
        const authorId = req.user._id;
        DialogModel.find({author: authorId})
            .populate(["author", "partner"])
            .exec((err, dialogs) => {
                if (err) {
                    return res.status(404).json({
                        message: 'Диалоги не найдены'
                    });
                }
                return res.json(dialogs);
            })
    }

    /**
     * Метод создает диалог. В БД заносится только author, partner, остальные значение, пришедшие
     * с req.body игнорируются
     * @param {express.Request} req - запрос
     * @param {express.Response} res - ответ
     */
    create(req: any, res: express.Response) {
        const postData = {
            author: req.user._id,
            partner: req.body.partner,
            text: req.body.text
        };

        const dialog = new DialogModel(postData);
        dialog.save().then((dialogObj: any) => {
            res.json(dialogObj);

            const message = new MessageModel({
                text: req.body.text,
                dialog: dialogObj._id,
                author: req.body.author
            });

            message.save().then((messageObj: any) => {
                res.json({
                    dialog: dialogObj
                });
            }).catch((err) => {
                res.json(err);
            })
        }).catch((err: any) => {
            res.json(err);
        })
    }

    /**
     * Метод удаляет диалог с БД. В случае успешного удаления получаем статус 200, неудачного - 404.
     * @param {express.Request} req - запрос
     * @param {express.Response} res - ответ
     */
    delete(req: express.Request, res: express.Response) {
        const _id: string = req.params._id;
        DialogModel.findOneAndRemove({_id})
            .then(dialog => {
                dialog && res.status(200).json({message: 'Диалог успешно удален'});
            }).catch(() => {
            res.json({message: 'Диалог не найден'})
        });
    }
}

export default DialogController;