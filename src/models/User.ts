/**
 * UserSchema - описывает объект, который хранится в документе "users" в БД "chat".
 */
import mongoose, {Schema, Document} from 'mongoose';
import isEmail from 'validator/lib/isEmail';
import {generateUserPassword} from '../utils/generateUserPassword';

/**
 * IUser нужен, чтобы обращаться к полям user-a, которого получаем в колбэке, например:
 * UserModel.findById(id, (err, user) => {
 *   return res.status(200).json({message: 'Мы нашли пользователя по имени ${user.fullname}'});
 * });
 */
export interface IUser extends Document{
    email: string,
    fullname: string,
    password: string,
    confirmed: boolean,
    avatar?: string,
    confirm_hash?: string,
    last_seen?: Date
}

const UserSchema = new Schema({
    email: {
        type: String,
        required: 'Email address is required',
        validate: [isEmail, 'неправильный email'],
        unique: true
    },
    fullname: {
        type: String,
        required: 'Fullname is required'
    },
    password: {
        type: String,
        required: 'Password is required'
    },
    confirmed: {
        type: Boolean,
        default: false
    },
    avatar: String,
    confirm_hash: String,
    last_seen: {
        type: Date,
        default: new Date()
    }
}, {
    timestamps: true // добавляет created_at, updated_at
});

UserSchema.pre('save', function(next) {
    const user: any = this;

    if (!user.isModified('password')) return next();

    generateUserPassword(user.password).then((hash) => {
        user.password = hash;
        next();
    }).catch((err) => {
        next(err);
    });
});

export default mongoose.model<IUser>('User', UserSchema);