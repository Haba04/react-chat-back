/**
 * DialogSchema - описывает объект, который хранится в документе "dialogs" в БД "chat".
 */
import mongoose, {Schema, Document} from 'mongoose';

/**
 * IDialog нужен, чтобы обращаться к полям user-a, которого получаем в колбэке, например:
 * DialogModel.findById(id, (err, dialog) => {
 *   return res.status(200).json({message: 'Мы нашли пользователя по имени ${dialog.fullname}'});
 * });
 */
export interface IDialog extends Document{
    author: {
        type: Schema.Types.ObjectId,
        ref: string,
        require: true
    },
    partner: {
        type: Schema.Types.ObjectId,
        ref: string,
        require: true
    },
    lastMessage: {
        type: Schema.Types.ObjectId,
        ref: string,
        require: true
    },
}

const DialogSchema = new Schema({
    author: {type: Schema.Types.ObjectId, ref: 'User'},
    partner: {type: Schema.Types.ObjectId, ref: 'User'},
    lastMessage: {type: Schema.Types.ObjectId, ref: 'Message'}
}, {
    timestamps: true // добавляет created_at, updated_at
});

export default mongoose.model<IDialog>('Dialog', DialogSchema);