/**
 * MessageSchema - описывает объект, который хранится в документе "messages" в БД "chat".
 */
import mongoose, {Schema, Document} from 'mongoose';

/**
 * IMessage нужен, чтобы обращаться к полям user-a, которого получаем в колбэке, например:
 * MessageModel.findById(id, (err, message) => {
 *   return res.status(200).json({message: 'Мы нашли автора диалога, его зовут ${message.author}'});
 * });
 */
export interface IMessage extends Document {
    text: {
        type: string,
        require: true
    },
    dialog: {
        type: Schema.Types.ObjectId,
        ref: string,
        require: true
    },
    author: {
        type: Schema.Types.ObjectId,
        ref: string,
        require: true
    },
    unread: {
        type: boolean,
        default: false
    },
}

// TODO: сделать аттач файлов
// attachements:
const MessageSchema = new Schema({
    text: {
        type: String,
        require: Boolean
    },
    dialog: {
        type: Schema.Types.ObjectId,
        ref: "Dialog",
        require: true
    },
    author: {
        type: Schema.Types.ObjectId,
        ref: "User",
        require: true
    },
    unread: {
        type: Boolean,
        default: false
    },
}, {
    timestamps: true // добавляет created_at, updated_at
});

export default mongoose.model<IMessage>('Message', MessageSchema);