import express from 'express';
import bodyParser from 'body-parser';
import socket from 'socket.io';
import {createServer} from 'http';

import mongooseConnection from "./utils/mongooseConnection";
import UserController from './controllers/UserController';
import DialogController from './controllers/DialogController';
import MessageController from './controllers/MessageController';
import updateLastSeen from './middleware/updateLastSeen';
import checkAuth from './middleware/checkAuth';
import {loginValidation} from "./utils/validations";
import config from './configs';

const app: any = express();
const http: any = createServer(app);
const io: any = socket(http);

const userController = new UserController();
const dialogController = new DialogController();
const messageController = new MessageController();

// middlewares
app.use(bodyParser.json());
app.use(updateLastSeen);
app.use(checkAuth);

mongooseConnection(); // подключение к БД

app.get('/users/me', userController.getMe);
app.get('/users/:_id', userController.show);
app.post('/users/registration', userController.create);
app.post('/users/login', loginValidation, userController.login);
app.delete('/users/:_id', userController.delete);

app.get('/dialogs/', dialogController.index);
app.post('/dialogs',dialogController.create);
app.delete('/dialogs/:_id', dialogController.delete);

app.get('/messages/:_id', messageController.index);
app.post('/messages/',messageController.create);
app.delete('/messages/:_id', messageController.delete);

io.on('connection', (socket: any) => {
    console.log(socket, 'Пользователь подключился');
});

process.on('uncaughtException', (err: Error) => {
    console.log(err);
});

http.listen(config.PORT, () => console.log(`Start on port ${config.PORT}`));