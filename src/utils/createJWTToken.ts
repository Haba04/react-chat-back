import jwt from 'jsonwebtoken';
import reduce from 'lodash/reduce';
import configs from '../configs';
import {IUser} from "../models/User";

export default (user: IUser) => {
    const token = jwt.sign(
        {
            data: reduce(
                user,
                (result: any, value, key) => {
                if (key !== "password") {
                    result[key] = value;
                }
                return result;
            }, {})
        },
        configs.JWT_SECRET,
        {
            expiresIn: configs.JWT_MAX_TIME,
            algorithm: "HS256"
        }
    );

    return token
}