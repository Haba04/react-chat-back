const bcrypt = require("bcrypt");

const pass = 'qwergfvnqoelrwghaqeg';
const rounds = 10;

function createHash(pass = 'qwergfvnqoelrwghaqeg', rounds = 10) {
    bcrypt.hash(pass, rounds, (err, hash) => {
        if (err) {
            console.log(err);
            return;
        }
        console.log(hash);
    });
}

function compareHash(pass = 'qwergfvnqoelrwghaqeg', hash = '$2b$10$oX0pj1647n7U3BvrThFy6epempl3xDTiQjKEOZgrjc57VhY53QD5C') {
    bcrypt.compare(pass, hash, (err, res) => {
        if (err) {
            console.log(err);
            return;
        }
        console.log(res);
    });
}

// createHash();
compareHash();