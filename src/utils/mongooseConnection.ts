import config from "../configs";
import mongoose from "mongoose";

export default () => {
    const db = mongoose.connection;
    mongoose.connect(config.MONGO_URL, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    });
    db.on('connect', () => console.log("connect db successfully"));
    db.on('error', () => console.error("connect db error"));
}