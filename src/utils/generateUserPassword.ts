import bcrypt from "bcrypt";
import configs from "../configs";

export const generateUserPassword = (password: string) => {
    return new Promise((resolve, reject) => {
        bcrypt.genSalt(configs.SALT_WORK_FACTOR, (err, salt) => {
            if (err) return reject(err);

            bcrypt.hash(password, salt, (err, hash) => {
                if (err) return reject(err);
                console.log('passwordHash:', hash);
                resolve(hash)
            });
        });
    })
};

export const compareUserPassword = (password: string, hash: string) => {
    return new Promise((resolve, reject) => {
        bcrypt.compare(password, hash, (err, res) => {
            if (err) return reject(err);
            resolve(res)
        });
    });
};
