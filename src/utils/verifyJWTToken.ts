import config from '../configs';
import {IUser} from "../models/User";
import {VerifyErrors} from "jsonwebtoken";
const jwt = require('jsonwebtoken');

export default (token: string) => {
    return new Promise((resolve, reject) => {
        jwt.verify(token, config.JWT_SECRET, (err: VerifyErrors, decodedData: IUser) => {
            if (err || !decodedData) {
                return reject(err);
            }
            resolve(decodedData);
        });
    });
}