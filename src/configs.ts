export default {
  "PORT": 4000,
  "JWT_SECRET": "FdCVRDAWdwasFDsqwa87FsdJFdD632HD",
  "MONGO_URL": "mongodb://127.0.0.1:27017/chat",
  "JWT_MAX_TIME": "7 days",
  "ALGORITHM": "HS256",
  "SALT_WORK_FACTOR": 10
}