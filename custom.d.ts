import {Request} from 'express'
import {IUser} from "./src/models/User";

export interface RequestCustom extends Request {
    user: IUser;
    headers: {
        token: string
    }
}